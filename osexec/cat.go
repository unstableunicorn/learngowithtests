package cat

import (
	"bytes"
	"encoding/xml"
	"io"
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
)

type Payload struct {
	Message string `xml:"message"`
}

func GetData(data io.Reader) string {
	var payload Payload
	if err := xml.NewDecoder(data).Decode(&payload); err != nil {
		log.Fatalf("failed to decode payload: %v", err)
	}
	return strings.ToUpper(payload.Message)
}

func getXMLFromCommand() io.Reader {
	cmd := exec.Command("cat", "msg.xml")
	out, _ := cmd.StdoutPipe()

	if err := cmd.Start(); err != nil {
		log.Fatalf("error starting: %v", err)
	}

	data, err := ioutil.ReadAll(out)
	if err != nil {
		log.Fatalf("failed read output: %v", err)
	}

	if err := cmd.Wait(); err != nil {
		log.Fatalf("error code: %v", err)
	}

	return bytes.NewReader(data)
}

func GetDataV1() string {
	cmd := exec.Command("cat", "msg.xml")

	out, _ := cmd.StdoutPipe()
	var payload Payload
	decoder := xml.NewDecoder(out)

	if err := cmd.Start(); err != nil {
		log.Fatalf("error starting: %v", err)
	}

	if err := decoder.Decode(&payload); err != nil {
		log.Fatalf("failed to decode payload: %v", err)
	}

	if err := cmd.Wait(); err != nil {
		log.Fatalf("error code: %v", err)
	}

	return strings.ToUpper(payload.Message)
}
