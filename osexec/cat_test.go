package cat

import (
	"strings"
	"testing"
)

func TestGetDataV1(t *testing.T) {
	got := GetDataV1()
	want := "HAPPY NEW YEAR!"

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func TestGetData(t *testing.T) {
	input := strings.NewReader(`
<payload>
    <message>Unicorns are the best creature</message>
</payload>`)

	got := GetData(input)
	want := "UNICORNS ARE THE BEST CREATURE"

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func TestGetDataIntegration(t *testing.T) {
	got := GetData(getXMLFromCommand())
	want := "HAPPY NEW YEAR!"

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}
