package structs

import "math"

// Shape is a shape duh
type Shape interface {
	Area() float64
}

// Rectangle holds the width and height
type Rectangle struct {
	Width  float64
	Height float64
}

// Circle holds the width and height
type Circle struct {
	Radius float64
}

// Triangle holds the width and height
type Triangle struct {
	Base   float64
	Height float64
}

// Area takes a Rectangle and returns the area
func (r Rectangle) Area() float64 {
	return r.Width * r.Height
}

// Area takes a Circle and returns the area
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Area takes a Triangle and returns the area
func (t Triangle) Area() float64 {
	return (t.Base * t.Height) * 0.5
}

// Perimeter takes a Rectangle and returns the perimeter
func (r Rectangle) Perimeter() float64 {
	return 2 * (r.Width + r.Height)
}
