package counter

import (
	"sync"
	"testing"
)

func TestCounter(t *testing.T) {
	t.Run("increment the counter n times leaves it at n", func(t *testing.T) {
		cases := []struct {
			Name string
			N    int
			Want int
		}{
			{"for three", 3, 3},
			{"for 10", 10, 10},
		}

		for _, test := range cases {
			t.Run(test.Name, func(t *testing.T) {
				counter := NewCounter()
				for i := 0; i < test.N; i++ {
					counter.Inc()
				}
				assertCounter(t, counter, test.Want)
			})
		}
	})

	t.Run("it runs safely concurrently", func(t *testing.T) {
		wantedCount := 1000
		counter := NewCounter()

		var wg sync.WaitGroup
		wg.Add(wantedCount)

		for i := 0; i < wantedCount; i++ {
			go func(w *sync.WaitGroup) {
				counter.Inc()
				w.Done()
			}(&wg)
		}

		wg.Wait()
		assertCounter(t, counter, wantedCount)
	})
}

func assertCounter(t *testing.T, got *Counter, want int) {
	t.Helper()
	if got.Value() != want {
		t.Errorf("got %d, expected %d", got.Value(), want)
	}
}
