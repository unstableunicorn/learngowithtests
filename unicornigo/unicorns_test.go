package main

import "testing"

func TestUnicorns(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("Spotting pretty unicorns", func(t *testing.T) {
		got := Unicorns("so pretty", "")
		want := "Unicorns, so pretty"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Say 'Unicorns, so magical' when an empty string is supplied", func(t *testing.T) {
		got := Unicorns("", "")
		want := "Unicorns, so magical"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Hola, en espanol!", func(t *testing.T) {
		got := Unicorns("muy hermosa", "Spanish")
		want := "Unicornio, muy hermosa"
		assertCorrectMessage(t, got, want)
	})

	t.Run("Hungry Hungarian Unicorns?", func(t *testing.T) {
		got := Unicorns("olyan éhesek", "Hungarian")
		want := "Egyszarvúak, olyan éhesek"
		assertCorrectMessage(t, got, want)
	})
}
