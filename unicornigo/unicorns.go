package main

import "fmt"

const spanish = "Spanish"
const hungarian = "Hungarian"
const englishUnicornPrefix = "Unicorns, "
const spanishUnicornPrefix = "Unicornio, "
const hungarianUnicornPrefix = "Egyszarvúak, "

// Unicorns hello
func Unicorns(statement, language string) string {
	if statement == "" {
		statement = "so magical"
	}
	return greetingPrefix(language) + statement
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case spanish:
		prefix = spanishUnicornPrefix
	case hungarian:
		prefix = hungarianUnicornPrefix
	default:
		prefix = englishUnicornPrefix
	}
	return
}

func main() {
	fmt.Println(Unicorns("everywhere!", ""))
}
