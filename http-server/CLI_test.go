package poker_test

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"testing"

	poker "gitlab.com/unstableunicorn/learngowithtests/http-server"
)

func TestCLI(t *testing.T) {
	plays := []struct {
		winner      string
		playerCount int
		multiplier  int
	}{
		{"Knight", 3, 120},
		{"Unicorn", 8, 60},
	}

	for _, p := range plays {
		t.Run(fmt.Sprintf("start game with %v players and finish game with %q as winner", p.playerCount, p.winner), func(t *testing.T) {
			game := &poker.GameSpy{}
			multiplier := 60
			stdout := &bytes.Buffer{}

			in := userSends(strconv.Itoa(p.playerCount), strconv.Itoa(multiplier), fmt.Sprintf("%v wins", p.winner))
			cli := poker.NewCLI(in, stdout, game)

			cli.PlayPoker()
			assertMessagesSentToUser(t, stdout, poker.PlayerPrompt, poker.BlindPrompt, poker.WinnerPrompt)
			assertGameStartedWith(t, game, p.playerCount, multiplier)
			assertFinishedCalledWith(t, game, p.winner)
		})

	}

	t.Run("it prints an error when a non numeric value is entered for number of players and does not start the game", func(t *testing.T) {
		stdout := &bytes.Buffer{}
		in := strings.NewReader("rainbow\n")

		game := &poker.GameSpy{}

		cli := poker.NewCLI(in, stdout, game)
		cli.PlayPoker()

		assertGameNotStarted(t, game)

		assertMessagesSentToUser(t, stdout, poker.PlayerPrompt, poker.BadPlayerInputErrMsg)

	})

	t.Run("it prints an error when a non numeric value is entered for time per blind and does not start the game", func(t *testing.T) {
		stdout := &bytes.Buffer{}
		in := strings.NewReader("1\nUnicorns\n")

		game := &poker.GameSpy{}

		cli := poker.NewCLI(in, stdout, game)
		cli.PlayPoker()

		assertGameNotStarted(t, game)

		assertMessagesSentToUser(t, stdout, poker.PlayerPrompt, poker.BlindPrompt, poker.BadBlindInputErrMsg)

	})

	t.Run("it prints an error when a the winner string does not match 'name wins'", func(t *testing.T) {
		game := &poker.GameSpy{}
		stdout := &bytes.Buffer{}

		in := userSends("6", "60", "Stabby the Unicorn skewered us")
		cli := poker.NewCLI(in, stdout, game)

		cli.PlayPoker()
		assertGameNotFinished(t, game)

		assertMessagesSentToUser(t, stdout, poker.PlayerPrompt, poker.BlindPrompt, poker.WinnerPrompt, poker.BadWinnerInputErrMsg)

	})
}

func userSends(inputs ...string) *strings.Reader {
	return strings.NewReader(strings.Join(inputs, "\n"))
}

func assertScheduledAlert(t *testing.T, got, want poker.ScheduledAlert) {
	t.Helper()
	if got.GetAmount() != want.GetAmount() {
		t.Errorf("got amount %d, want %d", got.GetAmount(), want.GetAmount())
	}
	if got.GetDuration() != want.GetDuration() {
		t.Errorf("got scheduled time of %v, want %v", got.GetDuration(), want.GetDuration())
	}
}

func assertMessagesSentToUser(t *testing.T, stdout *bytes.Buffer, messages ...string) {
	t.Helper()
	want := strings.Join(messages, "")
	got := stdout.String()
	if got != want {
		t.Errorf("got %q sent to stdout but expected %+v", got, messages)
	}
}

func assertGameNotStarted(t *testing.T, game *poker.GameSpy) {
	t.Helper()

	if game.StartCalled {
		t.Errorf("game should not have started")
	}
}

func assertGameNotFinished(t *testing.T, game *poker.GameSpy) {
	t.Helper()

	if game.FinishedCalled {
		t.Errorf("game should not have finished")
	}
}

func assertGameStartedWith(t *testing.T, game *poker.GameSpy, playerCount, multiplier int) {
	t.Helper()
	if game.StartedWith != playerCount {
		t.Errorf("game players %v, but wanted %v", game.StartedWith, playerCount)
	}

	if game.Multiplier != multiplier {
		t.Errorf("game multiplier with %v, but wanted %v", game.Multiplier, multiplier)
	}
}

func assertFinishedCalledWith(t *testing.T, game *poker.GameSpy, winner string) {
	t.Helper()
	if game.FinishedWith != winner {
		t.Errorf("got game winner %v, but wanted %v", game.FinishedWith, winner)
	}
}
