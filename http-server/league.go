package poker

import (
	"encoding/json"
	"fmt"
	"io"
)

// League is a list of Players
type League []Player

// NewLeague takes a reader and decodes into a list of Players
func NewLeague(rdr io.Reader) (League, error) {
	var league League
	err := json.NewDecoder(rdr).Decode(&league)
	if err != nil {
		err = fmt.Errorf("problem parsing league, %v", err)
	}

	return league, err
}

// Find searches the league for te named player and returns a pointer to that player
func (l League) Find(name string) *Player {
	for i, p := range l {
		if p.Name == name {
			return &l[i]
		}
	}
	return nil
}
