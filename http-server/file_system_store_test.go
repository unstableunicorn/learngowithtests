package poker

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestFileSystemStore(t *testing.T) {
	t.Run("/league from a reader", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Unicorn", "Wins":10},
			{"Name": "Knight", "Wins":6}]`)

		defer cleanFileDB()

		got := store.GetLeague()

		want := []Player{
			{"Unicorn", 10},
			{"Knight", 6},
		}

		AssertLeague(t, got, want)
		got = store.GetLeague()
		AssertLeague(t, got, want)
	})

	t.Run("get player score", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Unicorn", "Wins":10},
			{"Name": "Knight", "Wins":6}]`)

		defer cleanFileDB()

		got := store.GetPlayerScore("Unicorn")

		want := 10

		AssertInt(t, got, want)
	})

	t.Run("store wins for existings players", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Unicorn", "Wins":10},
			{"Name": "Knight", "Wins":6}]`)

		defer cleanFileDB()

		store.RecordWin("Unicorn")

		got := store.GetPlayerScore("Unicorn")

		want := 11
		AssertInt(t, got, want)

	})

	t.Run("returns 0 for players with no data", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Knight", "Wins":6}]`)

		defer cleanFileDB()

		got := store.GetPlayerScore("Unicorn")

		want := 0
		AssertInt(t, got, want)

	})

	t.Run("stores wins for new players", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Unicorn", "Wins":10},
			{"Name": "Knight", "Wins":6}]`)

		defer cleanFileDB()

		store.RecordWin("UnicornsRock777")

		got := store.GetPlayerScore("UnicornsRock777")

		want := 1
		AssertInt(t, got, want)

	})

	t.Run("works with an empty file", func(t *testing.T) {
		_, cleanFileDB := newTempFileSystemPlayStore(t, "")
		defer cleanFileDB()
	})

	t.Run("creates new file db", func(t *testing.T) {
		db, err := NewFileDataStore("testfile.tst")
		if err != nil {
			t.Errorf("could not create file ")
		}
		os.Remove(db.Name())
	})

	t.Run("league sorted by score", func(t *testing.T) {
		store, cleanFileDB := newTempFileSystemPlayStore(t, `[
			{"Name": "Unicorn", "Wins":10},
			{"Name": "Knight", "Wins":6},
			{"Name": "King", "Wins":77}]`)

		defer cleanFileDB()

		got := store.GetLeague()

		want := []Player{
			{"King", 77},
			{"Unicorn", 10},
			{"Knight", 6},
		}

		AssertLeague(t, got, want)
		got = store.GetLeague()
		AssertLeague(t, got, want)
	})
}

func createTempFile(t *testing.T, initialData string) (*os.File, func()) {
	t.Helper()
	tmpfile, err := ioutil.TempFile("", "test_filedb")

	if err != nil {
		t.Fatalf("could not create temp file %v", err)
	}

	_, err = tmpfile.Write([]byte(initialData))

	if err != nil {
		t.Fatalf("could not write to file %v", err)
	}

	_, err = tmpfile.Seek(0, 0)
	if err != nil {
		t.Fatalf("could not seek file %v", err)
	}

	removeFile := func() {
		tmpfile.Close()
		os.Remove(tmpfile.Name())
	}

	return tmpfile, removeFile

}

func newTempFileSystemPlayStore(t *testing.T, initialData string) (*FileSystemPlayerStore, func()) {
	t.Helper()
	tmpfile, removeFile := createTempFile(t, initialData)

	store, err := NewFileSystemPlayerStore(tmpfile)
	if err != nil {
		t.Fatalf("could not create file system store: %v", err)
	}
	return store, removeFile
}
