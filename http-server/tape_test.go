package poker

import (
	"io/ioutil"
	"testing"
)

func TestTape_Write(t *testing.T) {
	file, clean := createTempFile(t, "12345")
	defer clean()

	tape := &tape{file}

	_, err := tape.Write([]byte("abc"))

	if err != nil {
		t.Fatalf("could not write to file %v", err)
	}

	_, err = file.Seek(0, 0)

	if err != nil {
		t.Fatalf("could not seek file %v", err)
	}

	newFileContents, _ := ioutil.ReadAll(file)

	got := string(newFileContents)
	want := "abc"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
