package poker

import (
	"sync"
)

// NewInMemoryPlayerStore returns an InMemoryPlayerStore
func NewInMemoryPlayerStore() *InMemoryPlayerStore {
	return &InMemoryPlayerStore{
		[]Player{},
		sync.RWMutex{},
	}
}

// InMemoryPlayerStore stores the score information about players
type InMemoryPlayerStore struct {
	store League
	lock  sync.RWMutex
}

// GetPlayerScore takes the player name and returns their score
func (ims *InMemoryPlayerStore) GetPlayerScore(name string) int {
	ims.lock.Lock()
	defer ims.lock.Unlock()

	player := ims.GetLeague().Find(name)
	if player != nil {
		return player.Wins
	}
	return 0
}

// RecordWin takes the player name and increments their win counter
func (ims *InMemoryPlayerStore) RecordWin(name string) {
	ims.lock.Lock()
	defer ims.lock.Unlock()

	player := ims.GetLeague().Find(name)
	if player != nil {
		player.Wins++
	} else {
		ims.store = append(ims.store, Player{Name: name, Wins: 1})
	}
}

// GetLeague returns a list of all players
func (ims *InMemoryPlayerStore) GetLeague() League {
	return ims.store
}
