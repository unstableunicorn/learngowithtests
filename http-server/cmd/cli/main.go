package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	poker "gitlab.com/unstableunicorn/learngowithtests/http-server"
)

func main() {
	dbTypePtr := flag.String("db", "inmem", "type of db, choose from 'inmem'(default), 'mongo', 'file'")
	flag.Parse()

	var server *poker.PlayerServer
	var store poker.PlayerStore
	var err error
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	store, err = poker.GetStore(ctx, *dbTypePtr)

	if err != nil {
		log.Fatalf("could not create store: %v", err)
	}

	log.Printf("Running databse type %v", server.Name)

	fmt.Println("Let's play poker!")

	game := poker.NewGame(poker.BlindAlerterFunc(poker.Alerter), store)

	poker.NewCLI(os.Stdin, os.Stdin, game).PlayPoker()
}
