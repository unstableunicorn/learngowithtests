package main

import (
	"context"
	"flag"
	"fmt"

	"log"
	"net/http"

	poker "gitlab.com/unstableunicorn/learngowithtests/http-server"
)

const (
	port = 5000
)

func main() {
	dbTypePtr := flag.String("db", "inmem", "type of db, choose from 'inmem', 'mongo', 'file'")
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var server *poker.PlayerServer
	var store poker.PlayerStore
	var err error

	store, err = poker.GetStore(ctx, *dbTypePtr)
	if err != nil {
		log.Fatalf("could not create store: %v", err)
	}

	game := poker.NewGame(poker.BlindAlerterFunc(poker.Alerter), store)
	server, err = poker.NewPlayerServer(store, game, *dbTypePtr)

	if err != nil {
		log.Fatalf("could not create server: %v", err)
	}

	fmt.Printf("Running server on port %v...\n", port)

	if err := http.ListenAndServe(fmt.Sprintf(":%v", port), server); err != nil {
		log.Fatalf("could not listen on port %v %v", port, err)
	}
}
