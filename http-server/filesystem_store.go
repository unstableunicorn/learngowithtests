package poker

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
)

// NewFileDataStore takes a path to a file string and creates a new file for a file based db
func NewFileDataStore(path string) (*os.File, error) {
	db, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0666)
	return db, err
}

// NewFileSystemPlayerStore returns an FileSystemPlayerStore
func NewFileSystemPlayerStore(file *os.File) (*FileSystemPlayerStore, error) {

	if err := initialisePlayerDBFile(file); err != nil {
		return nil, fmt.Errorf("problem initialising player file db, %v", err)
	}

	league, err := NewLeague(file)

	if err != nil {
		return nil, fmt.Errorf("problem loading player store from file %s, %v", file.Name(), err)
	}

	return &FileSystemPlayerStore{
		json.NewEncoder(&tape{file}),
		league,
	}, nil
}

func initialisePlayerDBFile(file *os.File) error {
	_, err := file.Seek(0, 0)
	if err != nil {
		return fmt.Errorf("could not seek file %v", err)
	}

	info, err := file.Stat()
	if err != nil {
		return fmt.Errorf("problem getting file info from the file %s, %v", file.Name(), err)
	}

	// handle starting with empty file
	if info.Size() == 0 {
		_, err = file.Write([]byte("[]"))

		if err != nil {
			return fmt.Errorf("could not write to file %v", err)
		}

		_, err = file.Seek(0, 0)
		if err != nil {
			return fmt.Errorf("could not seek file %v", err)
		}
	}
	return nil
}

// FileSystemPlayerStore stores the score information about players
type FileSystemPlayerStore struct {
	database *json.Encoder
	league   League
}

// GetLeague returns a list of all players
func (f *FileSystemPlayerStore) GetLeague() League {
	sort.Slice(f.league, func(i, j int) bool {
		return f.league[i].Wins > f.league[j].Wins
	})
	return f.league
}

// GetPlayerScore takes the player name and returns their score
func (f *FileSystemPlayerStore) GetPlayerScore(name string) (wins int) {
	player := f.league.Find(name)
	if player != nil {
		return player.Wins
	}
	return 0
}

// RecordWin takes the player name and increments their win counter
func (f *FileSystemPlayerStore) RecordWin(name string) {
	player := f.league.Find(name)
	if player != nil {
		player.Wins++
	} else {
		f.league = append(f.league, Player{name, 1})
	}

	err := f.database.Encode(f.league)
	if err != nil {
		log.Printf("failed to encode to db %v", err)
	}
}
