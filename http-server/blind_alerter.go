package poker

import (
	"context"
	"fmt"
	"io"
	"sync"
	"time"
)

// ScheduledAlert store the alert settings
type ScheduledAlert struct {
	ctx    context.Context
	wg     *sync.WaitGroup
	at     time.Duration
	amount int
	to     io.Writer
}

// NewScheduledAlert create a new scheduled alert struct store
func NewScheduledAlert(ctx context.Context, wg *sync.WaitGroup, at time.Duration, amount int, to io.Writer) ScheduledAlert {
	return ScheduledAlert{
		ctx:    ctx,
		wg:     wg,
		at:     at,
		amount: amount,
		to:     to,
	}
}

func (s ScheduledAlert) String() string {
	return fmt.Sprintf("%d chips at %v", s.amount, s.at)
}

// GetAmount returns the amount that wasset
func (s ScheduledAlert) GetAmount() int {
	return s.amount
}

// GetDuration returns the duration that was set
func (s ScheduledAlert) GetDuration() time.Duration {
	return s.at
}

// BlindAlerter schedules alerts for blind amounts.
type BlindAlerter interface {
	ScheduleAlertAt(sa ScheduledAlert)
}

// BlindAlerterFunc allows you to implement BlindAlerter with a function.
type BlindAlerterFunc func(sa ScheduledAlert)

// ScheduleAlertAt is BlindAlerterFunc implementation of BlindAlerter.
func (a BlindAlerterFunc) ScheduleAlertAt(sa ScheduledAlert) {
	a(sa)
}

// Alerter will schedule alerts and print them to os.Stdout.
func Alerter(sa ScheduledAlert) {
	timeCh := make(chan int)
	timer := time.AfterFunc(sa.at, func() {
		timeCh <- sa.amount
	})
	go func(tCh chan (int), t *time.Timer, w *sync.WaitGroup) {
		defer w.Done()
		select {
		case amount := <-tCh:
			fmt.Fprintf(sa.to, "Blind is now %d\n", amount)
		case <-sa.ctx.Done():
			t.Stop()
		}
	}(timeCh, timer, sa.wg)
}
