package poker

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
)

const (
	//PlayerPrompt is the default prompt to enter players.
	PlayerPrompt = "Please enter the number of players: "
	//BadPlayerInputErrMsg is the default message for incorrect input of number of players.
	BadPlayerInputErrMsg = "Bad value received for number of players, please try again with a number\n"
	//BlindPrompt is the default prompt to enter blind time.
	BlindPrompt = "Please enter the seconds to increase blind per player: "
	//BadBlindInputErrMsg is the default message for incorrect input of bline time.
	BadBlindInputErrMsg = "Bad value received for blind time, please try again with a number\n"
	//WinnerPrompt is the default prompt to enter winner.
	WinnerPrompt = "Please enter the winner: "
	//BadWinnerInputErrMsg is the default message for incorrect input for winning player.
	BadWinnerInputErrMsg = "Bad value received for winning player, please use '<name> wins'\n"
)

// Game interface
type Game interface {
	Start(gOpts GameOptions)
	Finish(Winner string)
}

// GameOptions holds the game details
type GameOptions struct {
	ctx             context.Context
	wg              *sync.WaitGroup
	numberOfPlayers int
	blindMultiplier int
	to              io.Writer
}

// GetNumberOfPlayers returns the number of players initially set.
func (g GameOptions) GetNumberOfPlayers() int {
	return g.numberOfPlayers
}

// GetBlindMultiplier returns the blind multiplier initially set.
func (g GameOptions) GetBlindMultiplier() int {
	return g.blindMultiplier
}

// NewGameOptions creates a new game options struct store
func NewGameOptions(ctx context.Context, wg *sync.WaitGroup, numberOfPlayers int, blindMultiplier int, to io.Writer) GameOptions {
	return GameOptions{
		wg:              wg,
		ctx:             ctx,
		numberOfPlayers: numberOfPlayers,
		blindMultiplier: blindMultiplier,
		to:              to,
	}
}

// CLI holds a Player Store
type CLI struct {
	in   *bufio.Scanner
	out  io.Writer
	game Game
}

// NewCLI contructs a CLI from a PlayerStore and Reader
func NewCLI(in io.Reader, out io.Writer, game Game) *CLI {
	return &CLI{
		in:   bufio.NewScanner(in),
		out:  out,
		game: game,
	}
}

// PlayPoker starts the poker player interface
func (cli *CLI) PlayPoker() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var wg sync.WaitGroup
	setupCatchClose(cancel)
	fmt.Fprint(cli.out, PlayerPrompt)

	numberOfPlayersInput := cli.readLine()
	numberOfPlayers, err := strconv.Atoi(strings.Trim(numberOfPlayersInput, "\n"))
	if err != nil {
		fmt.Fprint(cli.out, BadPlayerInputErrMsg)
		return
	}

	fmt.Fprint(cli.out, BlindPrompt)
	blindMultiplierInput := cli.readLine()
	blindMultiplier, err := strconv.Atoi(strings.Trim(blindMultiplierInput, "\n"))
	if err != nil {
		fmt.Fprint(cli.out, BadBlindInputErrMsg)
		return
	}
	gOpts := NewGameOptions(ctx, &wg, numberOfPlayers, blindMultiplier, os.Stdout)

	// cli.game.Start(numberOfPlayers, blindMultiplier)
	cli.game.Start(gOpts)
	//wait for blinds to finish printing:
	wg.Wait()
	fmt.Fprint(cli.out, WinnerPrompt)
	winnerInput := cli.readLine()
	winner, err := extractWinner(winnerInput)
	if err != nil {
		fmt.Fprint(cli.out, BadWinnerInputErrMsg)
		return
	}
	cli.game.Finish(winner)
}

func (cli *CLI) readLine() string {
	cli.in.Scan()
	return cli.in.Text()
}

func setupCatchClose(cancel context.CancelFunc) {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("Stopping Game..")
		cancel()
	}()
}

func extractWinner(userInput string) (string, error) {
	input := strings.Split(userInput, " ")
	winString := strings.ToLower(input[len(input)-1])
	if winString != "wins" {
		return "", fmt.Errorf("input of winner did not end with 'wins'")
	}
	winner := strings.Join(input[:len(input)-1], " ")
	return winner, nil
}
