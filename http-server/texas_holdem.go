package poker

import (
	"time"
)

// TexasHoldem manages the state of the game.
type TexasHoldem struct {
	alerter BlindAlerter
	store   PlayerStore
}

// NewGame generates a new Game
func NewGame(alerter BlindAlerter, store PlayerStore) Game {
	return &TexasHoldem{
		alerter: alerter,
		store:   store,
	}
}

// Start ...
func (g *TexasHoldem) Start(gOpts GameOptions) {
	blindIncrement := time.Duration(gOpts.blindMultiplier*gOpts.numberOfPlayers) * time.Second
	blinds := []int{100, 200, 300, 400, 500, 600, 800, 1000, 2000, 4000, 8000}

	blindTime := 0 * time.Second
	gOpts.wg.Add(len(blinds))
	for _, blind := range blinds {
		sa := ScheduledAlert{
			at:     blindTime,
			amount: blind,
			wg:     gOpts.wg,
			ctx:    gOpts.ctx,
			to:     gOpts.to,
		}
		g.alerter.ScheduleAlertAt(sa)
		blindTime = blindTime + blindIncrement
	}
}

// Finish ...
func (g *TexasHoldem) Finish(winner string) {
	g.store.RecordWin(winner)
}
