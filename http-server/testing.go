package poker

import (
	"log"
	"net/http/httptest"
	"reflect"
	"sync"
	"testing"
)

// SpyBlindAlerter .
type SpyBlindAlerter struct {
	Alerts []ScheduledAlert
}

func (s *SpyBlindAlerter) ScheduleAlertAt(sa ScheduledAlert) {
	s.Alerts = append(s.Alerts, sa)
}

type GameSpy struct {
	StartCalled    bool
	StartedWith    int
	BlindAlert     []byte
	FinishedWith   string
	FinishedCalled bool
	Multiplier     int
	mutex          sync.Mutex
}

func (g *GameSpy) Start(gOpts GameOptions) {
	g.mutex.Lock()
	defer g.mutex.Unlock()
	g.StartedWith = gOpts.GetNumberOfPlayers()
	g.Multiplier = gOpts.GetBlindMultiplier()
	g.StartCalled = true
	_, err := gOpts.to.Write(g.BlindAlert)
	if err != nil {
		log.Fatalf("could not write to output %v", err)
	}
}

func (g *GameSpy) Finish(winner string) {
	g.mutex.Lock()
	defer g.mutex.Unlock()
	g.FinishedWith = winner
	g.FinishedCalled = true
}

type SpyWaitGroup struct {
	count int
}

func (swg *SpyWaitGroup) Add(delta int) {
	swg.count += delta
}

func (swg *SpyWaitGroup) Done() {
	swg.count--
}

func (swg *SpyWaitGroup) Wait() {
}

// StubPlayerStore is a PlayerStore for testing
type StubPlayerStore struct {
	scores   map[string]int
	winCalls []string
	league   League
}

// GetPlayerScore takes a player name and returns a score
func (s *StubPlayerStore) GetPlayerScore(name string) int {
	score := s.scores[name]
	return score
}

// RecordWin takes a player name and records a win
func (s *StubPlayerStore) RecordWin(name string) {
	s.winCalls = append(s.winCalls, name)
}

// GetLeague returns a list of all players
func (s *StubPlayerStore) GetLeague() League {
	return s.league
}

// AssertResponseBody tests the response from the server
func AssertResponseBody(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("response body is wrong, got %v want %v", got, want)
	}
}

// AssertStatus tests the status from the server
func AssertStatus(t *testing.T, got *httptest.ResponseRecorder, want int) {
	t.Helper()
	if got.Code != want {
		t.Errorf("did not get correct status, got %d, want %d", got.Code, want)
	}
}

// AssertContentType tests the content type of the response
func AssertContentType(t *testing.T, response *httptest.ResponseRecorder, want string) {
	t.Helper()
	if response.Result().Header.Get("content-type") != want {
		t.Errorf("response did not have content-type of %s, got %v", want, response.Result().Header)
	}
}

// AssertInt tests for matching ints
func AssertInt(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("got %d, want %d", got, want)
	}
}

// AssertLeague validates 2 Player lists
func AssertLeague(t *testing.T, got, want League) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}
