// +build integration

package poker

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestMongoDBSystemStore(t *testing.T) {
	t.Run("/league from a reader", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		store, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("integrationtest%v", rand.Intn(1000)), "testplayers")
		if err != nil {
			t.Errorf("could not create mongo db: %v", err)
		}
		defer store.client.Disconnect(ctx)
		defer store.database.Drop(ctx)

		initialiseMongoDBStore(t, store, []Player{
			{Name: "Unicorn", Wins: 10},
			{Name: "Knight", Wins: 6},
		})

		got := store.GetLeague()

		want := []Player{
			{"Unicorn", 10},
			{"Knight", 6},
		}

		AssertLeague(t, got, want)
		got = store.GetLeague()
		AssertLeague(t, got, want)
	})

	t.Run("get player score", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		store, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("integrationtest%v", rand.Intn(1000)), "testplayers")
		if err != nil {
			t.Errorf("could not create mongo db: %v", err)
		}
		defer store.client.Disconnect(ctx)
		defer store.database.Drop(ctx)

		initialiseMongoDBStore(t, store, []Player{
			{Name: "Unicorn", Wins: 10},
			{Name: "Knight", Wins: 6},
		})

		got := store.GetPlayerScore("Unicorn")

		want := 10

		AssertInt(t, got, want)
	})

	t.Run("store wins for existings players", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		store, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("integrationtest%v", rand.Intn(1000)), "testplayers")
		if err != nil {
			t.Errorf("could not create mongo db: %v", err)
		}
		defer store.client.Disconnect(ctx)
		defer store.database.Drop(ctx)

		initialiseMongoDBStore(t, store, []Player{
			{Name: "Unicorn", Wins: 10},
			{Name: "Knight", Wins: 6},
		})

		store.RecordWin("Unicorn")

		got := store.GetPlayerScore("Unicorn")

		want := 11
		AssertInt(t, got, want)

	})

	t.Run("stores wins for new players", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		store, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("integrationtest%v", rand.Intn(1000)), "testplayers")
		if err != nil {
			t.Errorf("could not create mongo db: %v", err)
		}
		defer store.client.Disconnect(ctx)
		defer store.database.Drop(ctx)

		initialiseMongoDBStore(t, store, []Player{
			{Name: "Unicorn", Wins: 10},
			{Name: "Knight", Wins: 6},
		})

		store.RecordWin("UnicornsRock777")

		got := store.GetPlayerScore("UnicornsRock777")

		want := 1
		AssertInt(t, got, want)

	})

	t.Run("league sorted by score", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		store, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("unittests%v", rand.Intn(1000)), "testplayers")
		if err != nil {
			t.Errorf("could not create mongo db: %v", err)
		}

		defer store.client.Disconnect(ctx)
		defer store.database.Drop(ctx)

		initialiseMongoDBStore(t, store, []Player{
			{Name: "Unicorn", Wins: 10},
			{Name: "Knight", Wins: 6},
			{Name: "King", Wins: 77},
		})

		got := store.GetLeague()

		want := []Player{
			{"King", 77},
			{"Unicorn", 10},
			{"Knight", 6},
		}

		AssertLeague(t, got, want)
		got = store.GetLeague()
		AssertLeague(t, got, want)
	})
}

func initialiseMongoDBStore(t *testing.T, store *MongoDBPlayerStore, players []Player) {
	t.Helper()
	for _, player := range players {
		_, err := store.playerCollection.InsertOne(store.ctx, player)
		if err != nil {
			t.Errorf("could not add player to mongod db, %v", err)
		}
	}
}
