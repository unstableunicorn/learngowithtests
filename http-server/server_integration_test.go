// +build integration

package poker

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"runtime"
	"testing"
	"time"
)

// print some information for test speed analysis
func init() {
	fmt.Printf("Number of Processors: %v\n", runtime.GOMAXPROCS(0))
	fmt.Printf("Number of CPU's: %v\n", runtime.NumCPU())
}

func TestRecordingWinsAndRetrievingThem(t *testing.T) {
	t.Parallel()
	// create player stores to test
	// In Memory Store
	inMemDBStore := NewInMemoryPlayerStore()
	// Empty File System Store
	fileDBStore, cleanFileFunc := newTempFileSystemPlayStore(t, "")
	defer cleanFileFunc()
	// Mongo DB Store
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	mongoDBStore, err := NewMongoDBPlayerStore(ctx, fmt.Sprintf("integrationtest%v", rand.Intn(1000)), "testplayers")
	if err != nil {
		t.Errorf("could not create mongo db: %v", err)
	}
	defer mongoDBStore.client.Disconnect(ctx)
	defer mongoDBStore.database.Drop(ctx)

	stores := []struct {
		store PlayerStore
		name  string
	}{
		{inMemDBStore, "In Mem Store"},
		{fileDBStore, "File Store"},
		{mongoDBStore, "MongoDB Store"},
	}

	for _, s := range stores {
		server, err := NewPlayerServer(s.store, dummyGame, s.name)
		if err != nil {
			t.Fatalf("could not get new player server %v", err)
		}

		players := []Player{
			{"Unicorn", 3},
			{"Knight", 2},
		}

		for _, p := range players {
			for i := 1; i <= p.Wins; i++ {
				server.ServeHTTP(httptest.NewRecorder(), newPostWinRequest(p.Name))
			}
			t.Run(fmt.Sprintf("get score: %v", s.name), func(t *testing.T) {
				response := httptest.NewRecorder()
				server.ServeHTTP(response, newGetScoreRequest(p.Name))
				AssertStatus(t, response, http.StatusOK)

				AssertResponseBody(t, response.Body.String(), fmt.Sprintf("%v", p.Wins))
			})
		}

		t.Run(fmt.Sprintf("get league: %v", s.name), func(t *testing.T) {
			response := httptest.NewRecorder()
			server.ServeHTTP(response, newLeagueRequest())
			AssertStatus(t, response, http.StatusOK)

			got := getLeagueFromResponse(t, response.Body)
			AssertLeague(t, got, players)
		})

	}
}
