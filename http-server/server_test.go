package poker

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

var (
	dummyGame        = &GameSpy{}
	dummyPlayerStore = &StubPlayerStore{}
	tenMilliSeconds  = 10 * time.Millisecond
)

func TestGetServer(t *testing.T) {
	cases := []struct {
		dbType string
	}{
		{"mongo"},
		{"file"},
		{"inmem"},
	}
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	for _, c := range cases {
		t.Run(fmt.Sprintf("getting %v db", c.dbType), func(t *testing.T) {
			store, err := GetStore(ctx, c.dbType)
			if err != nil {
				t.Errorf("Failed to get store: %v", err)
			}
			_, err = NewPlayerServer(store, dummyGame, "test")
			if err != nil {
				t.Errorf("Failed to get server: %v", err)
			}
		})
	}
}

func TestGETPlayers(t *testing.T) {
	store := StubPlayerStore{
		map[string]int{
			"Unicorn": 777,
			"Knight":  666,
		},
		nil,
		nil,
	}

	server, err := NewPlayerServer(&store, dummyGame, "test")

	if err != nil {
		t.Errorf("failed to create server %v", err)
	}

	cases := []struct {
		player string
		score  string
	}{
		{"Unicorn", "777"},
		{"Knight", "666"},
	}

	for _, c := range cases {
		t.Run(fmt.Sprintf("returns %v's score", c.player), func(t *testing.T) {
			req := newGetScoreRequest(c.player)
			res := httptest.NewRecorder()

			server.ServeHTTP(res, req)

			AssertStatus(t, res, http.StatusOK)
			AssertResponseBody(t, res.Body.String(), c.score)
		})

	}

	t.Run("returns 404 on missing players", func(t *testing.T) {
		req := newGetScoreRequest("Apollo")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)
		AssertStatus(t, res, http.StatusNotFound)
	})
}

func TestStoreWins(t *testing.T) {
	store := &StubPlayerStore{
		map[string]int{},
		nil,
		nil,
	}

	cases := []struct {
		player string
		score  string
	}{
		{"Unicorn", "777"},
	}

	server := mustMakePlayerServer(t, store, "test", dummyGame)

	for _, c := range cases {
		t.Run("ir records wins when POST", func(t *testing.T) {
			req := newPostWinRequest(c.player)
			res := httptest.NewRecorder()

			server.ServeHTTP(res, req)

			AssertStatus(t, res, http.StatusAccepted)

			AssertPlayerWins(t, store, c.player)
		})
	}
}

func TestLeague(t *testing.T) {
	t.Run("it returns the league table as JSON", func(t *testing.T) {
		wantedLeague := []Player{
			{"Unicorn", 777},
			{"Knight", 666},
			{"Dragon", 888},
		}

		store := &StubPlayerStore{nil, nil, wantedLeague}

		server := mustMakePlayerServer(t, store, "test", dummyGame)

		req := newLeagueRequest()
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		AssertContentType(t, res, jsonContentType)

		got := getLeagueFromResponse(t, res.Body)

		AssertStatus(t, res, http.StatusOK)

		AssertLeague(t, got, wantedLeague)

	})
}

func TestGame(t *testing.T) {
	t.Run("GET /game returns 200", func(t *testing.T) {
		store := &StubPlayerStore{nil, nil, nil}

		server := mustMakePlayerServer(t, store, "test", dummyGame)

		request := newGameRequest(t)
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		AssertStatus(t, response, http.StatusOK)
	})

	t.Run("start a game with 3 players, send some blind alerts down ws and declare Unicorn the winner", func(t *testing.T) {
		// when running this we do not care for log messages in the output.
		log.SetOutput(ioutil.Discard)

		wantedBlindAlert := "Blind is 100"

		game := &GameSpy{BlindAlert: []byte(wantedBlindAlert)}
		winner := "Ruth"
		server := httptest.NewServer(mustMakePlayerServer(t, dummyPlayerStore, "test", game))
		wsURL := "ws" + strings.TrimPrefix(server.URL, "http") + "/ws"
		ws := mustDialWS(t, wsURL)
		defer server.Close()
		defer ws.Close()

		writeWSMessage(t, ws, "3")
		writeWSMessage(t, ws, "1")
		writeWSMessage(t, ws, winner)

		assertGameStartedWith(t, game, 3, 1)
		assertFinishedCalledWith(t, game, winner)

		within(t, tenMilliSeconds, func() { assertWebsockerGotMsg(t, ws, wantedBlindAlert) })
	})
}

func mustDialWS(t *testing.T, url string) *websocket.Conn {
	t.Helper()
	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		t.Fatalf("could not open a ws connection on %s %v", url, err)
	}
	return ws
}

func writeWSMessage(t *testing.T, conn *websocket.Conn, message string) {
	t.Helper()
	if err := conn.WriteMessage(websocket.TextMessage, []byte(message)); err != nil {
		t.Fatalf("could not send message over ws connection %v", err)
	}
}

func getLeagueFromResponse(t *testing.T, b io.Reader) (league []Player) {
	league, err := NewLeague(b)

	if err != nil {
		t.Fatalf("unable to parse response from server %q into slice of Player, '%v'", b, err)
	}
	return
}

func newLeagueRequest() *http.Request {

	req, _ := http.NewRequest(http.MethodGet, "/league", nil)
	return req
}

func newGetScoreRequest(name string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/players/%v", name), nil)
	return req
}

func newPostWinRequest(name string) *http.Request {
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/players/%v", name), nil)
	return req
}

// AssertPlayerWins tests that a player win was recorded
func AssertPlayerWins(t *testing.T, store *StubPlayerStore, winner string) {
	t.Helper()

	if len(store.winCalls) != 1 {
		t.Fatalf("got %d calls to RecordWin want %d", len(store.winCalls), 1)
	}

	if store.winCalls[0] != winner {
		t.Errorf("did not store correct winner got %q want %q", store.winCalls[0], winner)
	}
}

func mustMakePlayerServer(t *testing.T, store PlayerStore, dbType string, game Game) *PlayerServer {
	t.Helper()
	server, err := NewPlayerServer(store, game, dbType)
	if err != nil {
		t.Errorf("failed to create server %v", err)
	}
	return server
}

func newGameRequest(t *testing.T) *http.Request {
	t.Helper()
	request, err := http.NewRequest(http.MethodGet, "/game", nil)
	if err != nil {
		t.Errorf("could not get game request: %v", err)
	}

	return request
}

func retryUntil(d time.Duration, f func() bool) bool {
	deadline := time.Now().Add(d)
	for time.Now().Before(deadline) {
		if f() {
			return true
		}
	}
	return false
}

func assertGameStartedWith(t *testing.T, game *GameSpy, playerCount, multiplier int) {
	t.Helper()

	passed := retryUntil(500*time.Millisecond, func() bool {
		game.mutex.Lock()
		defer game.mutex.Unlock()
		return game.StartedWith == playerCount
	})

	if !passed {
		t.Errorf("game players %v, but wanted %v", game.StartedWith, playerCount)
	}

	passed = retryUntil(500*time.Millisecond, func() bool {
		game.mutex.Lock()
		defer game.mutex.Unlock()
		return game.Multiplier == multiplier
	})

	if !passed {
		t.Errorf("game multiplier with %v, but wanted %v", game.Multiplier, multiplier)
	}
}

func assertFinishedCalledWith(t *testing.T, game *GameSpy, winner string) {
	t.Helper()

	passed := retryUntil(500*time.Millisecond, func() bool {
		game.mutex.Lock()
		defer game.mutex.Unlock()
		return game.FinishedWith == winner
	})

	if !passed {
		t.Errorf("got game winner %v, but wanted %v", game.FinishedWith, winner)
	}
}

func within(t *testing.T, d time.Duration, assert func()) {
	t.Helper()

	done := make(chan struct{}, 1)
	go func() {
		assert()
		done <- struct{}{}
	}()

	select {
	case <-time.After(d):
		t.Errorf("timed out")
	case <-done:
	}
}

func assertWebsockerGotMsg(t *testing.T, ws *websocket.Conn, want string) {
	_, msg, _ := ws.ReadMessage()
	if string(msg) != want {
		t.Errorf(`got "%s", want "%s"`, string(msg), want)
	}
}
