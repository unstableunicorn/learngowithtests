package poker

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	dbTimeout = 2 * time.Second
)

// NewMongoDBPlayerStore returns an InMemoryPlayerStore
func NewMongoDBPlayerStore(ctx context.Context, db, collection string) (*MongoDBPlayerStore, error) {
	dbURL, envExists := os.LookupEnv("MONGODB_URL")
	if !envExists {
		dbURL = "mongodb://mongodb:27017"
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(dbURL))
	if err != nil {
		log.Fatal(err)
	}

	if err := client.Connect(ctx); err != nil {
		return nil, fmt.Errorf("failed to connect to mongodb %v", err)
	}

	dbstore := client.Database(db)
	playerCollection := dbstore.Collection(collection)

	return &MongoDBPlayerStore{
		ctx,
		client,
		dbstore,
		playerCollection,
	}, nil
}

// MongoDBPlayerStore stores the score information about players
type MongoDBPlayerStore struct {
	ctx              context.Context
	client           *mongo.Client
	database         *mongo.Database
	playerCollection *mongo.Collection
}

// GetPlayerScore takes the player name and returns their score
func (db *MongoDBPlayerStore) GetPlayerScore(name string) int {
	ctx, cancel := context.WithTimeout(db.ctx, dbTimeout)
	defer cancel()
	var player Player
	if err := db.playerCollection.FindOne(ctx, bson.M{"name": name}).Decode(&player); err != nil {
		log.Fatal(err)
	}

	return player.Wins
}

// RecordWin takes the player name and increments their win counter
func (db *MongoDBPlayerStore) RecordWin(name string) {
	ctx, cancel := context.WithTimeout(db.ctx, dbTimeout)
	defer cancel()
	var player Player
	if err := db.playerCollection.FindOne(ctx, bson.M{"name": name}).Decode(&player); err != nil {
		if err.Error() == "mongo: no documents in result" {
			newPlayer := Player{Name: name, Wins: 1}
			_, err := db.playerCollection.InsertOne(ctx, newPlayer)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Fatal(err)
		}
	} else {
		player.Wins++
		_, err := db.playerCollection.UpdateOne(
			ctx,
			bson.M{"name": name},
			bson.D{{
				Key: "$set", Value: bson.D{{Key: "wins", Value: player.Wins}}},
			},
		)
		if err != nil {
			log.Fatal(err)
		}
	}
}

// GetLeague returns a list of all players sorted by win count ascending
func (db *MongoDBPlayerStore) GetLeague() League {
	ctx, cancel := context.WithTimeout(db.ctx, dbTimeout)
	defer cancel()
	var players League
	findOptions := options.Find()
	findOptions.SetSort(bson.M{"wins": -1})
	cursor, err := db.playerCollection.Find(ctx, bson.M{}, findOptions)
	if err != nil {
		log.Fatal(err)
	}
	if err := cursor.All(ctx, &players); err != nil {
		log.Fatal(err)
	}
	return players
}
