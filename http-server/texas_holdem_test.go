package poker_test

import (
	"context"
	"fmt"
	"os"
	"sync"
	"testing"
	"time"

	poker "gitlab.com/unstableunicorn/learngowithtests/http-server"
)

var (
	dummyGame        = &poker.GameSpy{}
	dummyPlayerStore = &poker.StubPlayerStore{}
)

func TestGame_Start(t *testing.T) {

	t.Run("it schedules alerts on game start for 5 players", func(t *testing.T) {
		blindAlerter := &poker.SpyBlindAlerter{}
		game := poker.NewGame(blindAlerter, dummyPlayerStore)

		ctx, cancel := context.WithCancel(context.Background())
		var wg sync.WaitGroup
		defer cancel()
		gOpts := poker.NewGameOptions(ctx, &wg, 5, 120, os.Stdout)

		game.Start(gOpts)

		cases := []poker.ScheduledAlert{
			poker.NewScheduledAlert(ctx, &wg, 0*time.Second, 100, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 10*time.Minute, 200, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 20*time.Minute, 300, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 30*time.Minute, 400, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 40*time.Minute, 500, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 50*time.Minute, 600, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 60*time.Minute, 800, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 70*time.Minute, 1000, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 80*time.Minute, 2000, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 90*time.Minute, 4000, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 100*time.Minute, 8000, os.Stdout),
		}

		checkScheduling(t, cases, blindAlerter)
	})

	t.Run("it schedules alerts on game start for 7 players", func(t *testing.T) {
		blindAlerter := &poker.SpyBlindAlerter{}
		game := poker.NewGame(blindAlerter, dummyPlayerStore)
		ctx, cancel := context.WithCancel(context.Background())
		var wg sync.WaitGroup
		defer cancel()
		gOpts := poker.NewGameOptions(ctx, &wg, 7, 120, os.Stdout)

		game.Start(gOpts)

		cases := []poker.ScheduledAlert{
			poker.NewScheduledAlert(ctx, &wg, 0*time.Second, 100, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 14*time.Minute, 200, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 28*time.Minute, 300, os.Stdout),
			poker.NewScheduledAlert(ctx, &wg, 42*time.Minute, 400, os.Stdout),
		}

		checkScheduling(t, cases, blindAlerter)

	})
}

func TestGame_Finish(t *testing.T) {
	game := dummyGame
	winner := "Ruth"

	game.Finish(winner)
	assertFinishedCalledWith(t, game, winner)
}

func checkScheduling(t *testing.T, cases []poker.ScheduledAlert, alerter *poker.SpyBlindAlerter) {
	t.Helper()
	for i, want := range cases {
		t.Run(fmt.Sprint(want), func(t *testing.T) {

			if len(alerter.Alerts) <= i {
				t.Fatalf("alert %d was not scheduled %v", i, alerter.Alerts)
			}

			got := alerter.Alerts[i]
			assertScheduledAlert(t, got, want)
		})
	}

}
