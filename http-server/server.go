package poker

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"text/template"

	"github.com/gorilla/websocket"
)

const (
	jsonContentType  = "application/json"
	fileDBName       = "game.db.json"
	htmlTemplatePath = "game.html"
)

// PlayerStore interface contains the methods required for player store implementations
type PlayerStore interface {
	GetPlayerScore(name string) int
	RecordWin(name string)
	GetLeague() League
}

// PlayerServer stores the store and Handler
type PlayerServer struct {
	Store    PlayerStore
	Name     string
	template *template.Template
	http.Handler
	game Game
}

// Player stores a single players details
type Player struct {
	Name string `json:"name" bson:"name"`
	Wins int    `json:"wins" bson:"wins"`
}

// NewPlayerServer takes a store and returns a pointer to a PlayerServer
func NewPlayerServer(store PlayerStore, game Game, name string) (*PlayerServer, error) {
	p := new(PlayerServer)

	tmpl, err := template.ParseFiles("game.html")

	if err != nil {
		return nil, fmt.Errorf("problem opening template %s %v", htmlTemplatePath, err)
	}

	p.template = tmpl

	p.Store = store
	p.game = game
	p.Name = name

	router := http.NewServeMux()
	router.Handle("/league", http.HandlerFunc(p.leagueHandler))
	router.Handle("/players/", http.HandlerFunc(p.playerHandler))
	router.Handle("/game", http.HandlerFunc(p.playGame))
	router.Handle("/ws", http.HandlerFunc(p.webSocket))
	p.Handler = router

	return p, nil
}

func (p *PlayerServer) leagueHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", jsonContentType)
	err := json.NewEncoder(w).Encode(p.Store.GetLeague())
	if err != nil {
		log.Printf("failed to encode league: %v", err)
	}
}

func (p *PlayerServer) playerHandler(w http.ResponseWriter, r *http.Request) {
	player := strings.TrimPrefix(r.URL.Path, "/players/")

	switch r.Method {
	case http.MethodPost:
		p.processWin(w, player)
	case http.MethodGet:
		p.showScore(w, player)
	}
}

func (p *PlayerServer) showScore(w http.ResponseWriter, player string) {
	w.Header().Set("content-type", jsonContentType)
	score := p.Store.GetPlayerScore(player)
	if score == 0 {
		w.WriteHeader(http.StatusNotFound)
	}

	fmt.Fprintf(w, "%v", score)
}

func (p *PlayerServer) processWin(w http.ResponseWriter, player string) {
	p.Store.RecordWin(player)
	w.WriteHeader(http.StatusAccepted)

}

func (p *PlayerServer) playGame(w http.ResponseWriter, r *http.Request) {
	err := p.template.Execute(w, nil)

	if err != nil {
		log.Printf("failed to execute template: %v", err)
	}
}

var wsUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type playerServerWS struct {
	*websocket.Conn
}

func newPlayerServerWS(w http.ResponseWriter, r *http.Request) *playerServerWS {
	conn, err := wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("problem upgrading connection to websocket %v\n", err)
	}

	return &playerServerWS{conn}
}

func (w *playerServerWS) WaitForMsg() string {
	_, msg, err := w.ReadMessage()
	if err != nil {
		log.Printf("error reading from websocket %v\n", err)
	}
	return string(msg)
}

func (w *playerServerWS) Write(p []byte) (n int, err error) {
	err = w.WriteMessage(websocket.TextMessage, p)

	if err != nil {
		return 0, err
	}

	return len(p), nil
}

func (p *PlayerServer) webSocket(w http.ResponseWriter, r *http.Request) {
	ws := newPlayerServerWS(w, r)

	numberOfPlayersMsg := ws.WaitForMsg()
	numberOfPlayers, _ := strconv.Atoi(numberOfPlayersMsg)
	log.Printf("recieved number of players of %v", numberOfPlayers)

	blindMultiplierMsg := ws.WaitForMsg()
	blindMultiplier, _ := strconv.Atoi(string(blindMultiplierMsg))
	log.Printf("recieved blind multiplier of %v", blindMultiplier)

	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	gOpts := NewGameOptions(ctx, &wg, numberOfPlayers, blindMultiplier, ws)
	p.game.Start(gOpts)

	winnerMsg := ws.WaitForMsg()
	log.Printf("recieved winner of %v", string(winnerMsg))
	p.game.Finish(string(winnerMsg))
}

// GetStore takes a type of db as a string and returns the server and db
func GetStore(ctx context.Context, dbType string) (PlayerStore, error) {
	var db PlayerStore
	var err error

	switch dbType {
	case "mongo":
		db, err = NewMongoDBPlayerStore(ctx, "httpserver", "players")
		if err != nil {
			return nil, fmt.Errorf("could not create mongo db: %v", err)
		}
	case "file":
		cwd, err := os.Getwd()
		if err != nil {
			return nil, fmt.Errorf("could not get current directory: %v", err)
		}
		file, err := NewFileDataStore(path.Join(cwd, fileDBName))
		if err != nil {
			return nil, fmt.Errorf("could not create file for db store: %v", err)
		}
		db, err = NewFileSystemPlayerStore(file)
		if err != nil {
			return nil, fmt.Errorf("could not create file player store: %v", err)
		}
	case "inmem":
		db = NewInMemoryPlayerStore()
	default:
		return nil, fmt.Errorf("option %q is an invalid db selection", dbType)
	}

	return db, err
}
