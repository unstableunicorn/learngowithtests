package reflection

import (
	"reflect"
	"testing"
)

type Unicorn struct {
	Name    string
	Profile Profile
}

type Profile struct {
	Power int
	Place string
}

func TestWalk(t *testing.T) {
	t.Run("test non maps", func(t *testing.T) {
		cases := []struct {
			Name          string
			Input         interface{}
			ExpectedCalls []string
		}{
			{
				"Struct with one string field",
				struct {
					Name string
				}{"Unicorn"},
				[]string{"Unicorn"},
			},
			{
				"Struct with two string fields",
				struct {
					Name string
					Type string
				}{"Unicorn", "Magical"},
				[]string{"Unicorn", "Magical"},
			},
			{
				"Struct with non string field",
				struct {
					Name string
					Age  int
				}{"Unicorn", 777},
				[]string{"Unicorn"},
			},
			{
				"nested fields",
				Unicorn{
					"Unicorn",
					Profile{777, "Magical Land"},
				},
				[]string{"Unicorn", "Magical Land"},
			},
			{
				"pointer to things",
				&Unicorn{
					"Unicorn",
					Profile{777, "Magical Land"},
				},
				[]string{"Unicorn", "Magical Land"},
			},
			{
				"slices",
				[]Profile{
					{777, "Magical Land"},
					{666, "Land of poop"},
				},
				[]string{"Magical Land", "Land of poop"},
			},
			{
				"arrays",
				[2]Profile{
					{777, "Magical Land"},
					{666, "Land of poop"},
				},
				[]string{"Magical Land", "Land of poop"},
			},
		}

		for _, test := range cases {
			t.Run(test.Name, func(t *testing.T) {
				var got []string
				walk(test.Input, func(input string) {
					got = append(got, input)
				})
				if !reflect.DeepEqual(got, test.ExpectedCalls) {
					t.Errorf("got %v, want %v", got, test.ExpectedCalls)
				}
			})
		}
	})

	t.Run("test maps", func(t *testing.T) {
		cases := []struct {
			Name          string
			Input         interface{}
			ExpectedCalls []string
		}{
			{
				"map the unicorn forwards",
				map[string]string{
					"666": "Land of poop",
					"777": "Magical Land",
				},
				[]string{"Land of poop", "Magical Land"},
			},
			{
				"map the unicorns backwards",
				map[string]string{
					"666": "Land of poop",
					"777": "Magical Land",
				},
				[]string{"Magical Land", "Land of poop"},
			},
		}

		for _, test := range cases {
			t.Run(test.Name, func(t *testing.T) {
				var got []string
				walk(test.Input, func(input string) {
					got = append(got, input)
				})

				if !assertUnOrderedArraysMatch(t, got, test.ExpectedCalls) {
					t.Errorf("got %v, want %v", got, test.ExpectedCalls)
				}

			})
		}

	})

	t.Run("with channels", func(t *testing.T) {
		λ := make(chan Profile)
		go func() {
			λ <- Profile{11, "λ"}
			λ <- Profile{110, "Λ"}
			close(λ)
		}()

		var got []string
		want := []string{"λ", "Λ"}
		walk(λ, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})

	t.Run("with functions", func(t *testing.T) {
		Λ := func() (Profile, Profile) {
			return Profile{77, "Unicorns"}, Profile{66, "Snrocinu"}
		}

		var got []string
		want := []string{"Unicorns", "Snrocinu"}

		walk(Λ, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})
}

func assertUnOrderedArraysMatch(t *testing.T, needles []string, haystack []string) bool {
	t.Helper()
	if len(needles) != len(haystack) {
		return false
	}

	countMatch := 0
	for _, needle := range needles {
		for _, x := range haystack {
			if x == needle {
				countMatch++
			}
		}
	}

	return countMatch == len(needles)
}
