package main

import (
	"bytes"
	"testing"
)

func TestGreet(t *testing.T) {
	t.Run("Say Hello Unicorn", func(t *testing.T) {
		buffer := bytes.Buffer{}
		Greet(&buffer, "Unicorn")
		got := buffer.String()
		want := "Hello Unicorn!"

		if got != want {
			t.Errorf("got %v, want %v", got, want)
		}
	})
}
