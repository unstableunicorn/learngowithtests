package main

import (
	"fmt"
	"io"
	"net/http"
)

func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello %s!", name)
}

func MyGreeterHandler(w http.ResponseWriter, r *http.Request) {
	Greet(w, "Unicorns!")
}

func main() {
	// Greet(os.Stdout, "Magical Unicorns")
	http.ListenAndServe(":6060", http.HandlerFunc(MyGreeterHandler))
}
