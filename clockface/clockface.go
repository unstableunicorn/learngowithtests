package clockface

import (
	"math"
	"time"
)

const (
	π                  = math.Pi
	secondsInHalfClock = 30
	secondsInClock     = 2 * secondsInHalfClock
	minutesInHalfClock = 30
	minutesInClock     = 2 * minutesInHalfClock
	hoursInHalfClock   = 6
	hoursInClock       = 2 * hoursInHalfClock
)

type Point struct {
	X, Y float64
}

func (p *Point) Scale(value float64) {
	p.X *= value
	p.Y *= value
}

func (p *Point) Flip() {
	p.Y = -p.Y
}

func (p *Point) Translate(x, y float64) {
	p.X += x
	p.Y += y
}

func makeHand(p Point, length float64) Point {
	p.Scale(length)
	p.Flip()
	p.Translate(clockCentreX, clockCentreY)
	return p
}

func secondHandPoint(t time.Time) Point {
	return angleToPoint(secondsInRadians(t))
}

func minuteHandPoint(t time.Time) Point {
	return angleToPoint(minutesInRadians(t))
}

func hourHandPoint(t time.Time) Point {
	return angleToPoint(hoursInRadians(t))
}

func angleToPoint(angle float64) Point {
	x := math.Sin(angle)
	y := math.Cos(angle)
	return Point{X: x, Y: y}
}

func secondsInRadians(t time.Time) (rSeconds float64) {
	return (π / (secondsInHalfClock / (float64(t.Second()))))
}

func minutesInRadians(t time.Time) float64 {
	return (secondsInRadians(t) / minutesInClock) + (π / (minutesInHalfClock / float64(t.Minute())))
}

func hoursInRadians(t time.Time) float64 {
	return (minutesInRadians(t) / hoursInClock) + (π / (hoursInHalfClock / float64(t.Hour()%hoursInClock)))
}
