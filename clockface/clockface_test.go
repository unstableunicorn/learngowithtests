package clockface

import (
	"math"
	"testing"
	"time"
)

func TestSecondsInRadians(t *testing.T) {
	cases := []struct {
		time  time.Time
		angle float64
	}{
		{simpleTime(0, 0, 30), π},
		{simpleTime(0, 0, 0), 0},
		{simpleTime(0, 0, 45), π / (30.0 / 45.0)},
		{simpleTime(0, 0, 7), π / (30.0 / 7.0)},
	}

	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := secondsInRadians(c.time)
			if !roughlyEqualFloat64(c.angle, got) {
				t.Fatalf("want %v radians, got %v", c.angle, got)
			}
		})
	}

}

func TestSecondHandPoint(t *testing.T) {
	cases := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(0, 0, 30), Point{0, -1}},
		{simpleTime(0, 0, 45), Point{-1, 0}},
	}
	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := secondHandPoint(c.time)
			if !roughlyEqualPoint(c.point, got) {
				t.Fatalf("want %v Point, got %v", c.point, got)
			}
		})
	}
}

func TestMinutesInRadians(t *testing.T) {
	cases := []struct {
		time  time.Time
		angle float64
	}{
		{simpleTime(0, 30, 0), π},
		{simpleTime(0, 0, 7), 7 * (π / (30 * 60))},
	}

	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := minutesInRadians(c.time)
			if !roughlyEqualFloat64(c.angle, got) {
				t.Fatalf("want %v radians, got %v", c.angle, got)
			}
		})
	}
}

func TestMinuteHandPoint(t *testing.T) {
	cases := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(0, 30, 0), Point{0, -1}},
		{simpleTime(0, 45, 0), Point{-1, 0}},
	}
	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := minuteHandPoint(c.time)
			if !roughlyEqualPoint(c.point, got) {
				t.Fatalf("want %v Point, got %v", c.point, got)
			}
		})
	}
}

func TestHoursInRadians(t *testing.T) {
	cases := []struct {
		time  time.Time
		angle float64
	}{
		{simpleTime(6, 0, 0), π},
		{simpleTime(0, 0, 0), 0},
		{simpleTime(21, 0, 0), π * 1.5},
		{simpleTime(0, 1, 30), π / ((6 * 60 * 60) / 90)},
	}

	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := hoursInRadians(c.time)
			if !roughlyEqualFloat64(c.angle, got) {
				t.Fatalf("want %v radians, got %v", c.angle, got)
			}
		})
	}
}

func TestHourHandPoint(t *testing.T) {
	cases := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(6, 0, 0), Point{0, -1}},
		{simpleTime(21, 0, 0), Point{-1, 0}},
	}
	for _, c := range cases {
		t.Run(testName(c.time), func(t *testing.T) {
			got := hourHandPoint(c.time)
			if !roughlyEqualPoint(c.point, got) {
				t.Fatalf("want %v Point, got %v", c.point, got)
			}
		})
	}
}

func simpleTime(hour, minute, second int) time.Time {
	return time.Date(1982, time.April, 7, hour, minute, second, 0, time.UTC)
}

func testName(tm time.Time) string {
	return tm.Format("Time is 15:04:05")
}

func roughlyEqualFloat64(a, b float64) bool {
	threshold := 1e-7
	return math.Abs(a-b) < threshold
}

func roughlyEqualPoint(a, b Point) bool {
	return roughlyEqualFloat64(a.X, b.X) && roughlyEqualFloat64(a.Y, b.Y)
}
