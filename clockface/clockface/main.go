package main

import (
	"os"
	"time"

	"gitlab.com/unstableunicorn/learngowithtests/clockface"
)

func main() {
	t := time.Now()
	clockface.SVGWriter(os.Stdout, t)
}
