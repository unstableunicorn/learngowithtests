package main

// Sum takes an array of integers and returns the sum of all of them
func Sum(numbers []int) int {
	sum := 0
	for _, i := range numbers {
		sum += i
	}

	return sum
}

// SumAll takes a varying amount of integer arrays and returns an array of the sum of each array
func SumAll(numbersToSum ...[]int) (sums []int) {
	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers))
	}
	return sums
}

// SumAllTails takes a varying amount of integer arrays and returns an array of the sum of the tail of each array
func SumAllTails(numbersToSum ...[]int) (sums []int) {
	for _, numbers := range numbersToSum {
		if len(numbers) == 0 {
			sums = append(sums, 0)
		} else {
			sums = append(sums, Sum(numbers[1:]))
		}
	}
	return sums
}

func main() {
}
