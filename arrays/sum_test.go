package main

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {

	t.Run("collection of any size", func(t *testing.T) {
		given := []int{1, 2, 3}

		got := Sum(given)
		want := 6
		if got != want {
			t.Errorf("got %d want %d given, %v", got, want, given)
		}
	})
}

func checkSums(t *testing.T, got, want []int) {
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestSumAll(t *testing.T) {
	t.Run("Test multiple slices", func(t *testing.T) {
		got := SumAll([]int{1, 2}, []int{0, 9})
		want := []int{3, 9}
		checkSums(t, got, want)
	})
}

func TestSumAllTails(t *testing.T) {
	t.Run("Testing the tails", func(t *testing.T) {
		got := SumAllTails([]int{1, 2}, []int{0, 9})
		want := []int{2, 9}

		checkSums(t, got, want)
	})

	t.Run("Testing empty slice", func(t *testing.T) {
		got := SumAllTails([]int{}, []int{0, 9}, []int{1, 2, 3})
		want := []int{0, 9, 5}

		checkSums(t, got, want)
	})
}
