package iteration

// Repeat takes a character string and a count and returns the string repeated the amount of count times
func Repeat(character string, count int) string {
	var repeated string
	for i := 0; i < count; i++ {
		repeated += character
	}

	return repeated
}
