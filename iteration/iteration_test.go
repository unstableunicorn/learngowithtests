package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	assertResult := func(t *testing.T, got, want string) {
		if got != want {
			t.Errorf("expected %q but got %q", want, got)
		}
	}

	t.Run("Repeat 'a' 5 times", func(t *testing.T) {
		got := Repeat("a", 5)
		want := "aaaaa"
		assertResult(t, got, want)
	})

	t.Run("Repeat 'unicorn' 3  times", func(t *testing.T) {
		got := Repeat("unicorn", 3)
		want := "unicornunicornunicorn"
		assertResult(t, got, want)
	})

}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

func ExampleRepeat() {
	repeatedString := Repeat("Unicorns", 4)
	fmt.Println(repeatedString)
	// Output: UnicornsUnicornsUnicornsUnicorns
}
