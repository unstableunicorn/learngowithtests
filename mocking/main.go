package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func main() {
	sleeper := &ConfigurableSleeper{1 * time.Second, time.Sleep}
	Countdown(os.Stdout, sleeper)
}

const weSee = "Unicorns!"
const countdownStart = 3

type Sleeper interface {
	Sleep()
}

type ConfigurableSleeper struct {
	duration time.Duration
	sleep    func(time.Duration)
}

func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

func writeOut(out io.Writer, a ...interface{}) {
	_, err := fmt.Fprintln(out, a...)
	if err != nil {
		log.Fatalf("Failed to print to %#v", out)
	}
}

func Countdown(out io.Writer, sleeper Sleeper) {
	for i := countdownStart; i > 0; i-- {
		sleeper.Sleep()
		writeOut(out, i)
	}

	sleeper.Sleep()
	writeOut(out, weSee)
}
