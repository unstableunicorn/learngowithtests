package pointers

import (
	"errors"
	"fmt"
)

// ErrInsufficientFunds is an appropriately named error
var ErrInsufficientFunds = errors.New("insufficient funds")

// Bitcoin is just a better int
type Bitcoin int

// Stringer strings things
type Stringer interface {
	String() string
}

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

// Wallet represent a bitcoin wallet
type Wallet struct {
	balance Bitcoin
}

// Deposit takes and amount and puts it in the wallet
func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

// Balance returns the Value of the wallet
func (w *Wallet) Balance() Bitcoin {
	return w.balance
}

// Withdraw reduces the Value of the wallet
func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}
	w.balance -= amount
	return nil
}
